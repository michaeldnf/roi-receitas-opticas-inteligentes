<?php
    session_start();
    require_once ('../class/cliente.php');
    if (isset($_POST['nome'])){
        //Tratamento
        $usuario = $_SESSION['id_usuario'];
        $nome    = trim($_POST['nome']);
        $sexo    = trim($_POST['sexo']);
        $data    = trim($_POST['datanasc']);
        $cpf     = trim($_POST['cpf']);
        $celular = trim($_POST['celular']);
        $obs     = trim($_POST['obs']);
//      ----------------------------------
        $cidade   = trim($_POST['sel-cidade']);
        $cep      = trim($_POST['cep']);
        $endereco = trim($_POST['endereco']);
        $numero   = trim($_POST['numero']);

        //Grava no banco
        $clt = new cliente();
        if ($clt->cadastrarEndereco($cidade,$cep,$endereco,$numero)){
            if ($clt->cadastrarCliente($nome,$sexo,$data,$cpf,$celular,$obs,$usuario)) {
                $_SESSION['cadCltTrue'] = 1;
            } else {$_SESSION['cadCltFalse'] = 0;}
        } else {$_SESSION['cadCltFalse'] = 0;}

        header('Location: ../pages/cadastro_cliente.php');
    }