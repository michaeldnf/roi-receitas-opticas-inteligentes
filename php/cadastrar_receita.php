<?php
    require_once ('../class/receita.php');
    $receita = new receita();

    //matriz dp
    $linha = 0;
    foreach ($_POST['dp-olho'] AS $olho){
        $dp[$linha]['olho'] = $olho;
        $linha++;
    }
    $linha = 0;
    foreach ($_POST['dp-visao'] AS $visao){
        $dp[$linha]['visao'] = $visao;
        $linha++;
    }
    $linha = 0;
    foreach ($_POST['dp'] AS $dnp){
        $dp[$linha]['dp'] = $dnp;
        $linha++;
    }

    //matriz adicao
    $linha = 0;
    foreach ($_POST['ad-olho'] AS $olho){
        $adicao[$linha]['olho'] = $olho;
        $linha++;
    }
    $linha = 0;
    foreach ($_POST['ad-visao'] AS $visao){
        $adicao[$linha]['visao'] = $visao;
        $linha++;
    }
    $linha = 0;
    foreach ($_POST['adicao'] AS $ad){
        $adicao[$linha]['adicao'] = $ad;
        $linha++;
    }

    //Tratamento dos valores da receita
    if($_POST['lodEsferico'] == "") {$lodEsferico = "-";} else{$lodEsferico = trim($_POST['lodEsferico']);}
    if($_POST['lodCilindrico'] == "") {$lodCilindrico = "-";} else{$lodCilindrico = trim($_POST['lodCilindrico']);}
    if($_POST['lodEixo'] == "") {$lodEixo = "-";} else{$lodEixo= trim($_POST['lodEixo']);}

    if($_POST['loeEsferico'] == "") {$loeEsferico = "-";} else{$loeEsferico = trim($_POST['loeEsferico']);}
    if($_POST['loeCilindrico'] == "") {$loeCilindrico = "-";} else{$loeCilindrico = trim($_POST['loeCilindrico']);}
    if($_POST['loeEixo'] == "") {$loeEixo = "-";} else{$loeEixo = trim($_POST['loeEixo']);}

    if($_POST['podEsferico'] == "") {$podEsferico = "-";} else{$podEsferico = trim($_POST['podEsferico']);}
    if($_POST['podCilindrico'] == "") {$podCilindrico = "-";} else{$podCilindrico = trim($_POST['podCilindrico']);}
    if($_POST['podEixo'] == "") {$podEixo = "-";} else{$podEixo = trim($_POST['podEixo']);}

    if($_POST['poeEsferico'] == "") {$poeEsferico = "-";} else{$poeEsferico = trim($_POST['poeEsferico']);}
    if($_POST['poeCilindrico'] == "") {$poeCilindrico = "-";} else{$poeCilindrico = trim($_POST['poeCilindrico']);}
    if($_POST['poeEixo'] == "") {$poeEixo = "-";} else{$poeEixo = trim($_POST['poeEixo']);}

    //Cadastro Banco
    $id_receita = $receita->cadastrarReceita(trim($_POST['cliente']),trim($_POST['data']),trim($_POST['problema']), trim($_POST['obs-doenca']),trim($_POST['lentes']), trim($_POST['preco']),trim($_POST['obs']),$lodEsferico,$lodCilindrico,$lodEixo,$loeEsferico,$loeCilindrico,$loeEixo,$podEsferico,$podCilindrico,$podEixo,$poeEsferico,$poeCilindrico,$poeEixo);
    $ok = true;
    if ($id_receita != false){
        foreach ($dp AS $d){
            if (($d['dp'] != '') and ($ok == true)){
                $ok = $receita->cadastrarDp($d['olho'],$d['visao'],$d['dp'],$id_receita);
            }
        }

        foreach ($adicao AS $ad){
            if (($ad['adicao'] != '') and ($ok == true)){
                $ok = $receita->cadastrarAdicao($ad['olho'],$ad['visao'],$ad['adicao'],$id_receita);
            }
        }
    }

    session_start();
    //Verifica se foi gravado no banco
    if (($id_receita == false) or ($ok == false)){
        $_SESSION['cadRctFalse'] = 0;
    } else {
        $_SESSION['cadRctTrue'] = 1;
    }
    header('Location: ../pages/cadastro_receita.php');
?>