<?php
    session_start();
    if(isset($_SESSION['usuario'])){
        header('Location: pages/home.php');
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="css/pageLogin.css">
        <link rel="stylesheet" href="css/estilo.css">
        <!-- ICONE -->
        <link rel="icon" href="img/favicon.png" sizes="16x16" type="image/png">
        <!-- TITULO DO SITE -->
        <title>Login - Receitas Ópticas Inteligentes</title>
    </head>
    <body class="bg-dark">
        <div class="alert alert-danger position-absolute hide" style="width: 100%" role="alert">
            Usuário e/ou Senha Incorreto(s)!
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-7 col-lg-5 container-login justify-content-center">
                    <span class="mx-auto d-block" style="margin-bottom: -15px">bem vindo</span>
                    <div class="mx-auto d-block p-2" style="margin-bottom: -10px">
                        <img class="img-fluid" src="img/logo-login.png">
                    </div>
                    <form class="col needs-validation" method="post" action="php/validar_acesso.php" novalidate>
                        <div class="form-group">
                            <label for="user">Usuário</label>
                            <input class="form-control" type="text" id="user" name="usuario" placeholder="Insira seu usuário" required>
                            <div class="invalid-feedback">
                                Insira um email válido
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="senha">Senha</label>
                            <input class="form-control" type="password" id="senha" name="senha" placeholder="Insira sua senha" required>
                            <div class="invalid-feedback">
                                Insira uma senha
                            </div>
                        </div>
<!--                        <div class="custom-control custom-checkbox">-->
<!--                            <input type="checkbox" class="custom-control-input" name="conectado" id="checkConnect">-->
<!--                            <label class="custom-control-label" for="checkConnect">Manter Conectado</label>-->
<!--                        </div>-->
                        <button class="btn btn-outline-info mx-auto d-block mt-3" type="submit">Entrar</button>

                    </form>
                </div>
            </div>
        </div>
        <!-- JavaScript (Opcional) -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="bootstrap/js/validate.js"></script>
        <script src="js/jquery/jquery-3.2.1.min.js"></script>
        <script>
            if (<?php if(isset($_SESSION['loginf'])){echo 'true'; unset($_SESSION['loginf']);} else {echo 'false';} ?>) {
                $('.alert').removeClass('hide');
                $('.alert').delay(2000).slideUp(1000);
            }
        </script>
        <script src="js/popper/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
