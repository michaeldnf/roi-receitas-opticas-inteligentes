<?php
    session_start();
    if((!isset($_SESSION['usuario']))){
        header('Location: ../index.php');
    }
    $id = $_GET['id'];
    require_once ('../class/conexaoBD.php');
    $bd = new conexaoBD();
    $link = $bd->conecta_mysql();
    $sql = "SELECT * FROM view_cliente_endereco WHERE id_cliente = $id";
    $result = mysqli_query($link,$sql);
    $result = mysqli_fetch_array($result);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="../css/estilo.css">
        <!-- ICONE -->
        <link rel="icon" href="../img/favicon.png" sizes="16x16" type="image/png">
        <!-- TITULO DO SITE -->
        <title>Detalhes do Cliente - Receitas Ópticas Inteligentes</title>
    </head>
    <body>

        <div class="alertS alert alert-success position-absolute hide" style="width: 100%" role="alert">
            Alterado com Sucesso!
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="alertE alert alert-danger position-absolute hide" style="width: 100%" role="alert">
            Erro ao Alterar!
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a href="home.php"><img src="../img/icon-text.png" class="img-fluid" width="125"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="home.php">Home</a>

                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" href="#" id="dropdownCadastro" role="button" data-toggle="dropdown">
                                Cadastro
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCadastro">
                                <a class="dropdown-item" href="cadastro_cliente.php">Clientes</a>
                                <a class="dropdown-item" href="cadastro_receita.php">Receitas</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item active" href="listar_cliente.php">Listar Clientes</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownPerfil" role="button" data-toggle="dropdown">
                                Perfil
                            </a>
                            <div class="dropdown-menu">
                                <h6 class="dropdown-header"><?php echo $_SESSION['nome']; ?></h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="perfil.php">Acessar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../php/sair.php">Sair</a>
                            </div>
                        </div>

                    </div>
                </div>

                <form class="form-group mt-3 md-hide" action="listar_cliente.php" method="get" style="width: 25%">
                    <div class="input-group input-group-sm  ">
                        <input class="form-control" type="search" name="s" placeholder="Buscar cliente...">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-info rounded-right">Procurar</button>
                        </div>
                    </div>
                </form>

            </div>
        </nav>
        <div class="container bg-light rounded shadow" style="margin-top: 100px">
            <div class="text-center pt-3 border-bottom pb-2 ">
                <h5>Detalhes do Cliente</h5>
            </div>
            <div class="text-right pt-3">
                <a role="button" class="btn btn-sm btn-outline-info" href="alterar_cliente.php?id=<?php echo $result['id_cliente']; ?>">Alterar</a>
                <button class="btn btn-sm btn-outline-danger" role='dialog' data-toggle='modal' data-target='#deleteConfirm'>Deletar</button>
                <div class='modal fade' id='deleteConfirm' tabindex='-1'>
                    <div class='modal-dialog' role='document'>
                        <div class='modal-content'>
                            <div class='modal-header'>
                                <h5 class='modal-title'>Tem certeza que deseja excluir este cliente?</h5>
                                <button type='button' class='close' data-dismiss='modal'>
                                    <span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-left text-danger">Todas as receitas deste cliente será apagada!</p>
                            </div>
                            <div class='modal-footer'>
                                <button type='button' class='btn btn-primary' data-dismiss='modal'>Não</button>
                                <a class='btn btn-danger' role='button' href='../php/deletar_cliente.php?id=<?php echo $result['id_cliente']; ?>'>Sim</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form class="pb-5 needs-validation" novalidate method="post" action="../php/cadastrar_cliente.php">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group col-md-3">
                            <label for="cod">Código</label>
                            <input type="text" class="form-control" disabled value="<?php echo $result['id_cliente']; ?>" id="cod">
                        </div>
                        <div class="form-group col-md-7 col-lg-9">
                            <label for="nome">Nome Completo</label>
                            <input type="text" class="form-control" name="nome" id="nome" disabled value="<?php echo $result['nome_cliente']; ?>">
                        </div>
                        <div class="form-group col-md-3 col-lg-6">
                            <label for="sexo">Sexo</label>
                            <select class="form-control" name="sexo" id="sexo" disabled>
                                <option value="">Selecione </option>
                                <option value="M" <?php if ($result['sexo'] == "M"){echo 'selected';}?>>Masculino</option>
                                <option value="F" <?php if ($result['sexo'] == "F"){echo 'selected';}?>>Feminino</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-lg-5">
                            <label for="data">Data de Nascimento</label>
                            <input type="date" class="form-control" name="datanasc" id="data" value="<?php echo $result['data_nascimento'];?>" disabled>
                        </div>
                        <div class="form-group col-md-4 col-lg-5">
                            <label for="cpf">CPF</label>
                            <input type="text" maxlength="14" placeholder="000.000.000-00" class="form-control" name="cpf" id="cpf" value="<?php echo $result['cpf'];?>" disabled>
                        </div>
                        <div class="form-group col-md-4 col-lg-5">
                            <label for="celular">Celular</label>
                            <input type="text" maxlength="13" placeholder="00 00000-0000" class="form-control" name="celular" id="celular" value="<?php echo $result['celular'];?>" disabled>
                        </div>
                    </div>
                    <div class="border-left col-lg-6">
                        <div class="form-group col-md-3 col-lg-4">
                            <label for="estado">Estado</label>
                            <select class="form-control" id="estado" disabled >
                                <option value="0"><?php echo $result['nome_estado'];?></option>
                                <option value="">Selecione</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-lg-9">
                            <label for="cidade">Cidade</label>
                            <select class="custom-select" name="sel-cidade" id="cidade" disabled >
                                <option value="0"><?php echo $result['nome_cidade'];?></option>
                                <option value="">Selecione</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-lg-4">
                            <label for="cep">CEP</label>
                            <input type="text"  maxlength="10" placeholder="00.000-000" class="form-control" name="cep" id="cep" value="<?php echo $result['cep'];?>" disabled >
                        </div>
                        <div class="form-group col-md-6 col-lg-9">
                            <label for="endereco">Endereço</label>
                            <input type="text" class="form-control" name="endereco" id="endereco" value="<?php echo $result['endereco'];?>" disabled>
                        </div>
                        <div class="form-group col-md-3 col-lg-3">
                            <label for="numero">Número</label>
                            <input type="text" maxlength="7" class="form-control" name="numero" id="numero" value="<?php echo $result['numero'];?>" disabled >
                        </div>
                        <div class="form-group col-md-7 col-lg-9">
                            <label for="obs">Observações</label>
                            <textarea class="form-control" name="obs"  id="obs" rows="3" disabled><?php echo $result['observacao'];?></textarea>
                        </div>
                    </div>
                </div>
                <div class="mt-5 text-center hide">
                    <button type="submit" class="btn btn-primary" style="width: 30%">Salvar</button>
                </div>
            </form>

            <div class="text-center pt-3 border-top pb-2 ">
                <h5>Receitas Cadastradas</h5>
            </div>

            <table class="mb-5 table table-responsive-sm border">
                <thead>
                    <tr>
                        <th scope="col">CÓDIGO</th>
                        <th scope="col">DATA</th>
                        <th scope="col">PROBLEMA OCULAR</th>
                        <th scope="col">PREÇO (R$)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        require_once ('../class/receita.php');
                        $rct = new receita();
                        $receita = $rct->buscarReceita($result['id_cliente']);
                        if ($receita == false){
                            echo "<tr style='background-color: #f3f3f3;'><td colspan='4' class='text-center'>Não foi encontrado nenhum registro</td></tr>";
                        } else {
                            foreach ($receita AS $r) {
                                echo "<tr class='coll' id='$r[0]' style='background-color: #f3f3f3;'>
                                        <th scope='row'>$r[0]</th>
                                        <td><input type='date' readonly class='form-control-plaintext' value='$r[1]' style='padding: 0;'></td>
                                        <td>$r[2]</td>
                                        <td>$r[5]</td>
                                      </tr>
                                      <tr class='hide $r[0]'>
                                         <td colspan='2'>
                                             <button class='btn btn-sm btn-outline-danger' role='dialog' data-toggle='modal' data-target='#deleteConfirm$r[0]'>Deletar</button>
                                             <div class='modal fade' id='deleteConfirm$r[0]' tabindex='-1'> 
                                                <div class='modal-dialog' role='document'>
                                                    <div class='modal-content'>
                                                      <div class='modal-header'>
                                                        <h5 class='modal-title'>Tem certeza que deseja excluir esta receita?</h5>
                                                        <button type='button' class='close' data-dismiss='modal'>
                                                          <span>&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class='modal-footer'>
                                                        <button type='button' class='btn btn-primary' data-dismiss='modal'>Não</button>
                                                        <a class='btn btn-danger' role='button' href='../php/deletar_receita.php?id=$r[0]&cl=$r[19]'>Sim</a>
                                                      </div>
                                                    </div>
                                                </div>
                                             </div>
                                             <table class=\"table table-sm table-responsive-sm mb-1 pb-0\" style='background-color: #F8F9FA; margin-top: -20px;'>
                                                <thead>
                                                    <tr class=\"text-center\">
                                                        <th class=\"border-0\"></th>
                                                        <th class=\"border-0\"></th>
                                                        <th class=\"border\">Esférico</th>
                                                        <th class=\"border\">Cilíndrico</th>
                                                        <th class=\"border\">Eixo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class=\"text-center\">
                                                        <th class=\"align-middle border\" rowspan=\"2\">LONGE</th>
                                                        <th class=\"align-middle border\">OD</th>
                                                        <td class=\"border\">$r[7]</td>
                                                        <td class=\"border\">$r[8]</td>
                                                        <td class=\"border\">$r[9]</td>
                                                    </tr>
                                                    <tr class=\"text-center\">
                                                        <th class=\"align-middle border\">OE</th>
                                                        <td class=\"border\">$r[10]</td>
                                                        <td class=\"border\">$r[11]</td>
                                                        <td class=\"border\">$r[12]</td>
                                                    </tr>
                                                    <tr class=\"text-center\">
                                                        <th class=\"align-middle border\" rowspan=\"2\">PERTO</th>
                                                        <th class=\"align-middle border\">OD</th>
                                                        <td class=\"border\">$r[13]</td>
                                                        <td class=\"border\">$r[14]</td>
                                                        <td class=\"border\">$r[15]</td>
                                                    </tr>
                                                    <tr class=\"text-center\">
                                                        <th class=\"align-middle border\">OE</th>
                                                        <td class=\"border\">$r[16]</td>
                                                        <td class=\"border\">$r[17]</td>
                                                        <td class=\"border\">$r[18]</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class=\"form-group\">
                                                <label for=\"lentes\">Lentes</label>
                                                <textarea class=\"form-control\" id=\"lentes\" rows=\"1\" name=\"lentes\" disabled>$r[4]</textarea>
                                            </div>
                                            <span>DP/DNP</span>
                                            <table class='table table-sm table-bordered table-striped table-responsive-sm' style='background-color: #F8F9FA;'>
                                                <thead>
                                                    <tr class='text-center'>
                                                        <th>OLHO</th>
                                                        <th>VISÃO</th>
                                                        <th>DP/DNP</th>
                                                    </tr>
                                                </thead>
                                                <tbody>";
                                                            $dnp = $rct->buscarDpReceita($r[0]);
                                                            if ($dnp == false){
                                                                echo "<td colspan='3' class='text-center'>Não possui DP/DNP</td>";
                                                            } else{
                                                                foreach ($dnp AS $d){
                                                                    echo "<tr class='text-center'>
                                                                            <td>$d[0]</td>
                                                                            <td>$d[1]</td>
                                                                            <td>$d[2]</td>
                                                                          </tr>";
                                                                }
                                                            }

                                                echo "</tbody>
                                            </table>
                                         </td>
                                         <td colspan='2'>
                                            <div class=\"form-group\">
                                                <label for=\"obs\">Observações</label>
                                                <textarea class=\"form-control\" id=\"obs\" name=\"obs\" rows=\"2\" disabled>$r[6]</textarea>
                                            </div>
                                            
                                            <div class=\"form-group\">
                                                <label for=\"doenças\">Se possui doença</label>
                                                <textarea class=\"form-control\" id=\"doenças\" rows=\"2\" name=\"obs-doenca\" disabled>$r[3]</textarea>
                                            </div>
                                            
                                            <span>Adições</span>
                                            <table class='table table-sm table-bordered table-striped table-responsive-sm' style='background-color: #F8F9FA;'>
                                                <thead>
                                                    <tr class='text-center'>
                                                        <th>OLHO</th>
                                                        <th>VISÃO</th>
                                                        <th>ADIÇÃO</th>
                                                    </tr>
                                                </thead>
                                                <tbody>";

                                                    $ad = $rct->buscarAdicaoReceita($r[0]);
                                                    if ($ad == false){
                                                        echo "<td colspan='3' class='text-center'>Não possui Adição</td>";
                                                    } else{
                                                        foreach ($ad AS $a){
                                                            echo "<tr class='text-center'>
                                                                    <td>$a[0]</td>
                                                                    <td>$a[1]</td>
                                                                    <td>$a[2]</td>
                                                                  </tr>";
                                                        }
                                                    }

                                            echo "</tbody>
                                            </table>
                                         </td>
                                      </tr>";
                            }
                        }
                    ?>

                    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Título do modal</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                    <button type="button" class="btn btn-primary">Salvar mudanças</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </tbody>
            </table>
            <span class="text-hide"> s </span>
        </div>


        <footer class="bg-dark text-light text-center container-fluid " style="margin-top: 40px">Sistema desenvolvido para fins acadêmicos<br> &copy; Copyright - 2018 By SOR</footer>
        <!-- JavaScript  -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="../bootstrap/js/validate.js"></script>
        <script src="../js/jquery/jquery-3.2.1.min.js"></script>
        <script>
            if (<?php if(isset($_SESSION['cadCltTrue'])){echo 'true'; unset($_SESSION['cadCltTrue']);} else {echo 'false';} ?>) {
                $('.alertS').removeClass('hide');
                $('.alertS').delay(1500).fadeOut(1000);
            }

            if (<?php if(isset($_SESSION['cadCltFalse'])){echo 'true'; unset($_SESSION['cadCltFalse']);} else {echo 'false';} ?>) {
                $('.alertE').removeClass('hide');
                $('.alertE').delay(1500).fadeOut(1000);
            }
        </script>
        <script src="../js/jquery.mask.js"></script>
        <script>
            $('.coll').click(function () {
                var id = '.' + this.id;
                $(id).fadeToggle(200);
            });
        </script>
        <script src="../js/popper/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>