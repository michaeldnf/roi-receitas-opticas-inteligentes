<?php
    session_start();
    if((!isset($_SESSION['usuario']))){
        header('Location: ../index.php');
    }
    $id = $_GET['id'];
    require_once ('../class/conexaoBD.php');
    $bd = new conexaoBD();
    $link = $bd->conecta_mysql();
    $sql = "SELECT * FROM view_cliente_endereco WHERE id_cliente = $id";
    $result = mysqli_query($link,$sql);
    $result = mysqli_fetch_array($result);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="../css/estilo.css">
        <!-- ICONE -->
        <link rel="icon" href="../img/favicon.png" sizes="16x16" type="image/png">
        <!-- TITULO DO SITE -->
        <title>Alterar Cliente - Receitas Ópticas Inteligentes</title>
    </head>
    <body>
        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a href="home.php"><img src="../img/icon-text.png" class="img-fluid" width="125"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="home.php">Home</a>

                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" href="#" id="dropdownCadastro" role="button" data-toggle="dropdown">
                                Cadastro
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCadastro">
                                <a class="dropdown-item active" href="cadastro_cliente.php">Clientes</a>
                                <a class="dropdown-item" href="cadastro_receita.php">Receitas</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="listar_cliente.php">Listar Clientes</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownPerfil" role="button" data-toggle="dropdown">
                                Perfil
                            </a>
                            <div class="dropdown-menu">
                                <h6 class="dropdown-header"><?php echo $_SESSION['nome']; ?></h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="perfil.php">Acessar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../php/sair.php">Sair</a>
                            </div>
                        </div>

                    </div>
                </div>

                <form class="form-group mt-3 md-hide" action="listar_cliente.php" method="get" style="width: 25%">
                    <div class="input-group input-group-sm  ">
                        <input class="form-control" type="search" name="s" placeholder="Buscar cliente...">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-info rounded-right">Procurar</button>
                        </div>
                    </div>
                </form>

            </div>
        </nav>
        <div class="container bg-light rounded shadow" style="margin-top: 100px">
            <div class="text-center pt-3 border-bottom pb-2 ">
                <h5>Alterar Cliente</h5>
            </div>
            <form class="pb-5 pt-4 needs-validation" novalidate method="post" action="../php/update_cliente.php">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group col-md-3">
                            <label for="cod">Código</label>
                            <input type="text" class="form-control" readonly name="codigo" value="<?php echo $result['id_cliente']; ?>" id="cod">
                        </div>
                        <div class="form-group col-md-7 col-lg-9">
                            <label for="nome">Nome Completo<span class="text-danger"> *</span></label>
                            <input type="text" class="form-control" required name="nome" id="nome" value="<?php echo $result['nome_cliente']; ?>">
                            <div class="invalid-feedback">
                                Por favor insira o nome completo
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-lg-6">
                            <label for="sexo">Sexo<span class="text-danger"> *</span></label>
                            <select class="form-control" name="sexo" id="sexo" required>
                                <option value="">Selecione </option>
                                <option value="M" <?php if ($result['sexo'] == "M"){echo 'selected';}?>>Masculino</option>
                                <option value="F" <?php if ($result['sexo'] == "F"){echo 'selected';}?>>Feminino</option>
                            </select>
                            <div class="invalid-feedback">
                                Por favor selecione um sexo
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-lg-5">
                            <label for="data">Data de Nascimento<span class="text-danger"> *</span></label>
                            <input type="date" class="form-control" name="datanasc" id="data" required value="<?php echo $result['data_nascimento'];?>">
                            <div class="invalid-feedback">
                                Por favor insira uma data válida
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-lg-5">
                            <label for="cpf">CPF<span class="text-danger"> *</span></label>
                            <input type="text" maxlength="14" placeholder="000.000.000-00" required class="form-control" name="cpf" id="cpf" value="<?php echo $result['cpf'];?>">
                            <div class="invalid-feedback">
                                Por favor insira o CPF
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-lg-5">
                            <label for="celular">Celular<span class="text-danger"> *</span></label>
                            <input type="text" maxlength="13" required placeholder="00 00000-0000" class="form-control" name="celular" id="celular" value="<?php echo $result['celular'];?>" >
                            <div class="invalid-feedback">
                                Por favor insira um celular
                            </div>
                        </div>
                    </div>
                    <div class="border-left col-lg-6">
                        <div class="form-group col-md-3 col-lg-4">
                            <label for="estado">Estado<span class="text-danger"> *</span></label>
                            <select class="form-control" id="estado" required>
                                <option value="">Selecione</option>
                                <?php
                                    $_SESSION['estado'] = $result['nome_estado'];
                                    require_once ("../php/carregar_estado.php");
                                    unset($_SESSION['estado']);
                                ?>
                            </select>
                            <div class="invalid-feedback">
                                Por favor selecione um estado
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-9">
                            <label for="cidade">Cidade<span class="text-danger"> *</span></label>
                            <select class="custom-select" name="sel-cidade" id="cidade" required>
                                <option value="">Selecione</option>
                                <?php
                                    $_SESSION['cidade'] = $result['nome_cidade'];
                                ?>
                            </select>
                            <div class="invalid-feedback">
                                Por favor selecione uma cidade
                            </div>
                        </div>
                        <div class="form-group col-md-4 col-lg-4">
                            <label for="cep">CEP<span class="text-danger"> *</span></label>
                            <input type="text" maxlength="10" required placeholder="00.000-000" class="form-control" name="cep" id="cep" value="<?php echo $result['cep'];?>">
                            <div class="invalid-feedback">
                                Por favor insira um CEP
                            </div>
                        </div>
                        <div class="form-group col-md-6 col-lg-9">
                            <label for="endereco">Endereço<span class="text-danger"> *</span></label>
                            <input type="text" class="form-control" required name="endereco" id="endereco" value="<?php echo $result['endereco'];?>" >
                            <div class="invalid-feedback">
                                Por favor insira um endereço
                            </div>
                        </div>
                        <div class="form-group col-md-3 col-lg-3">
                            <label for="numero">Número<span class="text-danger"> *</span></label>
                            <input type="text" maxlength="7" required class="form-control" name="numero" id="numero" value="<?php echo $result['numero'];?>"  >
                            <div class="invalid-feedback">
                                Por favor insira um número
                            </div>
                        </div>
                        <div class="form-group col-md-7 col-lg-9">
                            <label for="obs">Observações</label>
                            <textarea class="form-control" name="obs"  id="obs" rows="3"><?php echo $result['observacao'];?></textarea>
                        </div>
                    </div>
                </div>
                <div class="mt-5 text-center">
                    <button type="submit" class="btn btn-primary" style="width: 30%">Salvar</button>
                </div>
            </form>
        </div>


    <footer class="bg-dark text-light text-center container-fluid " style="margin-top: 40px">Sistema desenvolvido para fins acadêmicos<br> &copy; Copyright - 2018 By SOR</footer>
    <!-- JavaScript  -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="../bootstrap/js/validate.js"></script>
    <script src="../js/jquery/jquery-3.2.1.min.js"></script>

    <script src="../js/jquery.mask.js"></script>
    <script src="../js/popper/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#cpf').mask('000.000.000-00');
            $('#celular').mask('00 00000-0000');
            $('#cep').mask('00.000-000');
            $('#estado').click();
        });

        $('#estado').after().click(function () {
            var estado = $('#estado').val();
            $('#cidade').load('../php/carregar_cidade.php',{uf: estado});
        });
    </script>

    </body>
</html>