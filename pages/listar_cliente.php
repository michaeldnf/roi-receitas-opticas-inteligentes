<?php
    session_start();
    if((!isset($_SESSION['usuario']))){
        header('Location: ../index.php');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="../css/estilo.css">
        <!-- ICONE -->
        <link rel="icon" href="../img/favicon.png" sizes="16x16" type="image/png">
        <!-- TITULO DO SITE -->
        <title>Listar Clientes - Receitas Ópticas Inteligentes</title>
    </head>
    <body>
        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
        <div class="container">
            <a href="home.php"><img src="../img/icon-text.png" class="img-fluid" width="125"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="home.php">Home</a>

                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active " href="#" id="dropdownCadastro" role="button" data-toggle="dropdown">
                            Cadastro
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownCadastro">
                            <a class="dropdown-item" href="cadastro_cliente.php">Clientes</a>
                            <a class="dropdown-item" href="cadastro_receita.php">Receitas</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item active" href="listar_cliente.php">Listar Clientes</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdownPerfil" role="button" data-toggle="dropdown">
                            Perfil
                        </a>
                        <div class="dropdown-menu">
                            <h6 class="dropdown-header"><?php echo $_SESSION['nome']; ?></h6>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="perfil.php">Acessar Perfil</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../php/sair.php">Sair</a>
                        </div>
                    </div>

                </div>
            </div>

            <form class="form-group mt-3 md-hide" action="listar_cliente.php" method="get" style="width: 25%">
                <div class="input-group input-group-sm  ">
                    <input class="form-control" type="search" name="s" placeholder="Buscar cliente...">
                    <div class="input-group-prepend">
                        <button type="submit" class="btn btn-info rounded-right">Procurar</button>
                    </div>
                </div>
            </form>

        </div>
        </nav>
        <div class="container bg-light rounded shadow" style="margin-top: 100px">
            <div class="text-center pt-3 border-bottom pb-2 ">
                <h5>Listar Clientes</h5>
            </div>
            <form class="form-group mb-5 mt-3">
                <div class="input-group input-group-lg">
                    <input class="form-control" id="busca" type="text"  placeholder="Buscar cliente...">
                    <div class="input-group-prepend">
                        <button type="button" id="btn-busca" class="btn btn-outline-secondary rounded-right">Procurar</button>
                    </div>
                </div>
            </form>

            <table class="mb-5 table table-hover table-responsive-sm border">
                <thead>
                <tr>
                    <th scope="col">CÓDIGO</th>
                    <th scope="col">NOME</th>
                    <th scope="col">CPF</th>
                    <th scope="col">TELEFONE</th>
                </tr>
                </thead>
                <tbody id="result-cliente"></tbody>
            </table>
            <div class="text-hide" style="height: 230px">s</div>
        </div>


        <footer class="bg-dark text-light text-center container-fluid" style="margin-top: 40px">Sistema desenvolvido para fins acadêmicos<br> &copy; Copyright - 2018 By SOR</footer>
        <!-- JavaScript (Opcional) -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="../bootstrap/js/validate.js"></script>
        <script src="../js/jquery/jquery-3.2.1.min.js"></script>
        <script src="../js/popper/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script>
            $('#busca').keyup(function () {
                $('#btn-busca').click();
                if ( event.which == 13 ) {
                    return false;
                }
            });

            $(document).ready(function () {
                $('#busca').val('<?php if(isset($_GET['s'])) echo $_GET['s']; ?>');
                $('#btn-busca').click();
            });

            $('#btn-busca').click(function () {
                var busca = $('#busca').val();
                $('#result-cliente').load('../php/carregar_cliente.php',{s: busca});
            });
        </script>

    </body>
</html>