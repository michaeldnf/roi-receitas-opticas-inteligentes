<?php
    session_start();
    if((!isset($_SESSION['usuario']))){
        header('Location: ../index.php');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="../css/estilo.css">
        <!-- ICONE -->
        <link rel="icon" href="../img/favicon.png" sizes="16x16" type="image/png">
        <!-- TITULO DO SITE -->
        <title>Perfil - Receitas Ópticas Inteligentes</title>
    </head>
    <body>
        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a href="home.php"><img src="../img/icon-text.png" class="img-fluid" width="125"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="home.php">Home</a>

                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownCadastro" role="button" data-toggle="dropdown">
                                Cadastro
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCadastro">
                                <a class="dropdown-item" href="cadastro_cliente.php">Clientes</a>
                                <a class="dropdown-item" href="cadastro_receita.php">Receitas</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="listar_cliente.php">Listar Clientes</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" href="#" id="dropdownPerfil" role="button" data-toggle="dropdown">
                                Perfil
                            </a>
                            <div class="dropdown-menu">
                                <h6 class="dropdown-header"><?php echo $_SESSION['nome']; ?></h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item active" href="perfil.php">Acessar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../php/sair.php">Sair</a>
                            </div>
                        </div>

                    </div>
                </div>

                <form class="form-group mt-3 md-hide" action="listar_cliente.php" method="get" style="width: 25%">
                    <div class="input-group input-group-sm  ">
                        <input class="form-control" type="search" name="s" placeholder="Buscar cliente...">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-info rounded-right">Procurar</button>
                        </div>
                    </div>
                </form>

            </div>
        </nav>

        <div class="container bg-light rounded shadow " style="margin-top: 100px">
              <div class="d-flex col justify-content-center" style="min-height: 510px">
                  <div class="flex-fill align-self-center col-lg-6">
                      <?php if(((isset($_SESSION['al_usuario']))&&($_SESSION['al_usuario'] == 1))){echo '<h6 class="text-success">Dados Cadastrados com sucesso!</h6>';} ?>
                      <?php if(((isset($_SESSION['al_senha']))&&($_SESSION['al_senha'] == 1))){echo '<h6 class="text-success">Senha alterada com sucesso!</h6>';} ?>
                      <?php if(((isset($_SESSION['al_senha']))&&($_SESSION['al_senha'] == 2))){echo '<h6 class="text-danger">Senhas não conferem!</h6>';} ?>
                      <?php if(((isset($_SESSION['al_senha']))&&($_SESSION['al_senha'] == 3))){echo '<h6 class="text-danger">Senha atual inválida!</h6>';} ?>
                      <form class="needs-validation" novalidate action="../php/alterar_usuario.php" method="post">
                        <div class="text-right mt-5"><button type="button" class="btn btn-sm btn-outline-info btn-edit">Editar</button></div>
                        <div class="form-group">
                            <label for="nome">Nome do Usuário</label>
                            <input class="form-control" type="text" name="nome" id="nome" value="<?php echo $_SESSION['nome']; ?>" disabled required>
                            <div class="invalid-feedback">
                                Por Favor insira um nome
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user">Usuário</label>
                            <input class="form-control" type="text" name="usuario" id="user" value="<?php echo $_SESSION['usuario']; ?>" disabled required>
                            <div class="invalid-feedback">
                                Por Favor insira um usuário
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="senha">Mudar Senha</label>
                            <div class="input-group">
                                <input class="form-control" type="password" id="senha" name="senha-atual" placeholder="Senha Atual" disabled>
                                <input class="form-control" type="password" name="senha-nova" placeholder="Nova Senha" disabled>
                                <input class="form-control" type="password" name="repetir-senha" placeholder="Repita a Senha" disabled>
                            </div>
                        </div>
                          <div class="text-center mt-5"><button class="btn btn-primary" hidden>Salvar Alterações</button></div>
                      </form>
                  </div>
              </div>
        </div>

        <?php unset($_SESSION['al_usuario']); unset($_SESSION['al_senha']);?>
        <footer class="bg-dark text-light text-center container-fluid fixed-bottom" style="margin-top: 40px">Sistema desenvolvido para fins acadêmicos<br> &copy; Copyright - 2018 By SOR</footer>
        <!-- JavaScript (Opcional) -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="../bootstrap/js/validate.js"></script>
        <script src="../js/jquery/jquery-3.2.1.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.btn-edit').click(function () {
                    $('.form-group input').prop("disabled", false);
                    $('.btn').prop("hidden", false);
                });
            });
        </script>
        <script src="../js/popper/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>