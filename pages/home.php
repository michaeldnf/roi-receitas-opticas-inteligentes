    <?php
    session_start();
    if((!isset($_SESSION['usuario']))){
        header('Location: ../index.php');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS -->
        <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="../css/estilo.css">
        <!-- ICONE -->
        <link rel="icon" href="../img/favicon.png" sizes="16x16" type="image/png">
        <!-- TITULO DO SITE -->
        <title>Home - Receitas Ópticas Inteligentes</title>
    </head>
    <body>
        <div class="alert alert-success position-absolute hide" style="width: 100%" role="alert">
            Logado com sucesso!
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a href="home.php"><img src="../img/icon-text.png" class="img-fluid logo" width="125"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="home.php">Home</a>

                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownCadastro" role="button" data-toggle="dropdown">
                                Cadastro
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCadastro">
                                <a class="dropdown-item" href="cadastro_cliente.php">Clientes</a>
                                <a class="dropdown-item" href="cadastro_receita.php">Receitas</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="listar_cliente.php">Listar Clientes</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownPerfil" role="button" data-toggle="dropdown">
                                Perfil
                            </a>
                            <div class="dropdown-menu">
                                <h6 class="dropdown-header"><?php echo $_SESSION['nome']; ?></h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="perfil.php">Acessar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../php/sair.php">Sair</a>
                            </div>
                        </div>

                    </div>
                </div>

                <form class="form-group mt-3 md-hide" action="listar_cliente.php" method="get" style="width: 25%">
                    <div class="input-group input-group-sm  ">
                        <input class="form-control" type="search" name="s" placeholder="Buscar cliente...">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-info rounded-right">Procurar</button>
                        </div>
                    </div>
                </form>

            </div>
        </nav>

        <div class="container bg-light rounded shadow" style="margin-top: 100px">
            <div class="text-center">
                    <div class="border-bottom">
                        <a class="btn btn-primary mt-4 mb-4" role="button" href="cadastro_cliente.php">Cadastrar Cliente</a>
                        <a class="btn btn-info mt-4 mb-4" role="button" href="cadastro_receita.php">Cadastrar Receita</a>
                        <a class="btn btn-primary mt-4 mb-4" role="button" href="perfil.php">Acessar Perfil</a>
                    </div>
                    <form class="form-group mb-5 mt-3" action="listar_cliente.php" method="get">
                        <div class="input-group input-group-lg">
                            <input class="form-control" id="busca" type="text" name="s" placeholder="Buscar cliente...">
                            <div class="input-group-prepend">
                                <button type="submit" id="btn-busca" class="btn btn-outline-secondary rounded-right">Procurar</button>
                            </div>
                        </div>
                    </form>
                    <table class="mt-2 mb-5 table table-hover table-responsive-sm border">
                        <thead>
                        <tr class="text-center">
                            <th colspan="4">Últimos Clientes Cadastrados</th>
                        </tr>
                        <tr>
                            <th scope="col">CÓDIGO</th>
                            <th scope="col">NOME</th>
                            <th scope="col">CPF</th>
                            <th scope="col">TELEFONE</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                                require_once ('../class/cliente.php');
                                $clt = new cliente();
                                $result = $clt->ultimosCadastros(5);
                                foreach ($result AS $r){
                                    echo "<tr onclick=\"location.href='cliente.php?id=$r[0]'\">
                                            <th scope='row'>$r[0]</th>
                                            <td>$r[1]</td>
                                            <td>$r[2]</td>
                                            <td>$r[3]</td>
                                          </tr>";
                                }
                            ?>
                        </tbody>
                    </table>

                    <table class="mt-5 mb-5 table table-hover table-responsive-sm border">
                        <thead>
                            <tr class="text-center">
                                <th colspan="4">Receitas Vencidas</th>
                            </tr>
                            <tr>
                                <th scope="col">CÓDIGO</th>
                                <th scope="col">NOME</th>
                                <th scope="col">TELEFONE</th>
                                <th scope="col">DATA DA RECEITA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                require ('../php/receita_vencida.php');
                            ?>
                        </tbody>
                    </table>
                <span class="text-hide"> s </span>
            </div>
        </div>


        <footer class="bg-dark text-light text-center container-fluid" style="margin-top: 40px">Sistema desenvolvido para fins acadêmicos<br> &copy; Copyright - 2018 By SOR</footer>
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="../bootstrap/js/validate.js"></script>
        <script src="../js/jquery/jquery-3.2.1.min.js"></script>
        <script>
            if (<?php if(isset($_SESSION['loginok'])){echo 'true'; unset($_SESSION['loginok']);} else {echo 'false';} ?>) {
                $('.alert').removeClass('hide');
                $('.alert').delay(800).fadeOut(1000);
            }
        </script>
        <script src="../js/popper/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>