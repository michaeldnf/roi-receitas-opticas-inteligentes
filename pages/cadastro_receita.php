<?php
    session_start();
    if((!isset($_SESSION['usuario']))){
        header('Location: ../index.php');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS -->
    <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="../css/estilo.css">
    <link type="text/css" rel="stylesheet" href="../bootstrap-chosen/bootstrap-chosen.css">
    <!-- ICONE -->
    <link rel="icon" href="../img/favicon.png" sizes="16x16" type="image/png">
    <!-- TITULO DO SITE -->
    <title>Cadastro de Receitas - Receitas Ópticas Inteligentes</title>
</head>
    <body>

        <div class="alertS alert alert-success position-absolute hide" style="width: 100%" role="alert">
            Cadastrado com Sucesso!
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="alertE alert alert-danger position-absolute hide" style="width: 100%" role="alert">
            Erro ao Cadastrar!
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <nav class="navbar fixed-top navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a href="home.php"><img src="../img/icon-text.png" class="img-fluid" width="125"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="home.php">Home</a>
        
                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" href="#" id="dropdownCadastro" role="button" data-toggle="dropdown">
                                Cadastro
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCadastro">
                                <a class="dropdown-item" href="cadastro_cliente.php">Clientes</a>
                                <a class="dropdown-item active" href="cadastro_receita.php">Receitas</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="listar_cliente.php">Listar Clientes</a>
                            </div>
                        </div>
                        <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdownPerfil" role="button" data-toggle="dropdown">
                                Perfil
                            </a>
                            <div class="dropdown-menu">
                                <h6 class="dropdown-header"><?php echo $_SESSION['nome']; ?></h6>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="perfil.php">Acessar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../php/sair.php">Sair</a>
                            </div>
                        </div>

                    </div>
                </div>


                <form class="form-group mt-3 md-hide" action="listar_cliente.php" method="get" style="width: 25%">
                    <div class="input-group input-group-sm  ">
                        <input class="form-control" type="search" name="s" placeholder="Buscar cliente...">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-info rounded-right">Procurar</button>
                        </div>
                    </div>
                </form>

            </div>
        </nav>
        <div class="container bg-light rounded shadow" style="margin-top: 100px">
            <div class="text-center pt-3 border-bottom pb-2 ">
                <h5>Cadastro de Receitas</h5>
            </div>
            <form class="pb-5 pt-4 needs-validation" novalidate action="../php/cadastrar_receita.php" method="post">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group col-lg-8">
                            <label for="cliente">Selecione o Cliente <span class="text-danger"> *</span></label>
                            <select class="chosen-select form-control" name="cliente" id="cliente" required>
                                <option value=""></option>
                                <?php
                                    require_once ('../class/conexaoBD.php');
                                    $bd = new conexaoBD();
                                    $link  = $bd->conecta_mysql();
                                    $usuario_id = $_SESSION['id_usuario'];
                                    $sql = "SELECT id_cliente, nome_cliente FROM tb_cliente WHERE usuario_id = $usuario_id";
                                    $result = mysqli_query($link,$sql);
                                    $result = mysqli_fetch_all($result);
                                    foreach ($result AS $r){
                                        echo "<option value='$r[0]'>$r[1]</option>";
                                    }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                                Por Favor selecione um cliente
                            </div>
                        </div>
                        <div class="form-group col-lg-5">
                            <label for="data">Data da Receita <span class="text-danger"> *</span></label>
                            <input class="form-control" id="data" name="data" type="date" required>
                            <div class="invalid-feedback">
                                Por Favor insira uma data válida
                            </div>
                        </div>
                        <div class="form-group col-lg-8">
                            <label for="problema">Problema Ocular <span class="text-danger"> *</span></label>
                            <select class="form-control" name="problema" id="problema" required>
                                <option value="">Selecione</option>
                                <option value="Astigmatismo">Astigmatismo</option>
                                <option value="Miopia">Miopia</option>
                                <option value="Hipermetropia">Hipermetropia</option>
                                <option value="Ametropia">Ametropia</option>
                                <option value="Presbiopia">Presbiopia</option>
                            </select>
                            <div class="invalid-feedback">
                                Por Favor selecione uma opção
                            </div>
                        </div>
                        <div class="form-group col-lg-8">
                            <label for="doenças">Possui alguma doença?<span class="text-danger"> *</span></label>
                            <textarea class="form-control" id="doenças" rows="3" name="obs-doenca" required>Não Possui...</textarea>
                            <div class="invalid-feedback">
                                Por Favor não deixe vazio
                            </div>
                        </div>
                        <div class="form-group col-lg-8">
                            <label for="lentes">Lentes<span class="text-danger"> *</span></label>
                            <textarea class="form-control" id="lentes" rows="3" name="lentes" required>Simples</textarea>
                            <div class="invalid-feedback">
                                Por Favor insira o tipo de lente
                            </div>
                        </div>
                        <div class="form-group col-lg-8">
                            <label for="vlr">Preço R$</label>
                            <input class="form-control" id="vlr" maxlength="7" type="text" name="preco" placeholder="Preço (Opcional)">
                        </div>
                        <div class="form-group col-lg-8">
                            <label for="obs">Observações</label>
                            <textarea class="form-control" id="obs" name="obs" rows="3"></textarea>
                        </div>

                    </div>
                    <div class="col-lg-6 border-left">
                        <h6 class="text-center mt-2">Insira os Dados na Receita</h6>
                        <table class="table table-sm table-responsive-sm mb-1 pb-0">

                            <thead>
                                <tr class="text-center">
                                    <th class="border-0"></th>
                                    <th class="border-0"></th>
                                    <th class="border">Esférico</th>
                                    <th class="border">Cilíndrico</th>
                                    <th class="border">Eixo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <th class="align-middle border" rowspan="2">LONGE</th>
                                    <th class="align-middle border">OD</th>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="lodEsferico" placeholder="Ex: +1.25"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="lodCilindrico"  placeholder="-"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="lodEixo" placeholder="-"></td>
                                </tr>
                                <tr class="text-center">
                                    <th class="align-middle border">OE</th>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="loeEsferico" placeholder="Ex: +1.25"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="loeCilindrico" placeholder="-"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="loeEixo" placeholder="-"></td>
                                </tr>
                                <tr class="text-center">
                                    <th class="align-middle border" rowspan="2">PERTO</th>
                                    <th class="align-middle border">OD</th>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="podEsferico" placeholder="Ex: +1.25"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="podCilindrico" placeholder="-"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="podEixo" placeholder="-"></td>
                                </tr>
                                <tr class="text-center">
                                    <th class="align-middle border">OE</th>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="poeEsferico" placeholder="Ex: +1.25"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="poeCilindrico" placeholder="-"></td>
                                    <td class="border"><input class="form-control placeholdercenter text-center" type="text" name="poeEixo" placeholder="-"></td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="text-danger" style="font-size: 13px;">*Insira apenas os dados necessários</span>

                        <div class="mt-4">
                            <span class="text-muted text-center">
                                <h6 class="mt-5">DP/DNP</h6>
                            </span>
                            <div class="border-bottom mb-3"></div>

                            <ul class="form-group text-center addDP-DNP hide list-unstyled">
                                <div class="input-group col mb-1 text-muted">
                                    <span class="text-center col">Olho</span>
                                    <span class="text-center col">Visão</span>
                                    <span class="text-center col">DP/DNP</span>
                                </div>
                                <div class="input-group col mb-3">
                                    <select class="form-control col" name="dp-olho[]">
                                        <option value="OE e OD">OE e OD</option>
                                        <option value="OE">OE</option>
                                        <option value="OD">OD</option>
                                    </select>
                                    <select class="form-control col" name="dp-visao[]">
                                        <option value="Todos">Todos</option>
                                        <option value="Longe">Longe</option>
                                        <option value="Perto">Perto</option>
                                    </select>
                                    <input class="form-control col" type="text" name="dp[]" placeholder="DP/DNP">
                                </div>
                                <?php
                                $dp =   '<li class="input-group col mb-3">'.
                                    '<select class="form-control col" name="dp-olho[]">'.
                                    '<option value="OE e OD">OE e OD</option>'.
                                    '<option value="OE">OE</option>'.
                                    '<option value="OD">OD</option>'.
                                    '</select>'.
                                    '<select class="form-control col" name="dp-visao[]">'.
                                    '<option value="Todos">Todos</option>'.
                                    '<option value="Longe">Longe</option>'.
                                    '<option value="Perto">Perto</option>'.
                                    '</select>'.
                                    '<input class="form-control col" type="text" name="dp[]" placeholder="DP/DNP">'.
                                    '</li>';
                                ?>
                            </ul>
                            <div class="text-center">
                                <button type="button" class="btn btn-sm btn-outline-success mb-3 addDP">Adicionar</button>
                                <button type="button" class="btn btn-sm btn-outline-danger mb-3 rmvDP">Remover</button>
                            </div>

                        </div>

                        <div class="mt-4">
                            <span class="text-muted text-center">
                                <h6 class="mt-5">Adição</h6>
                            </span>
                            <div class="border-bottom mb-3"></div>

                            <ul class="form-group text-center addAdicao hide list-unstyled">
                                <div class="input-group col mb-1 text-muted">
                                    <span class="text-center col">Olho</span>
                                    <span class="text-center col">Visão</span>
                                    <span class="text-center col">Adição</span>
                                </div>
                                <div class="input-group col mb-3">
                                    <select class="form-control col" name="ad-olho[0]">
                                        <option value="OE e OD">OE e OD</option>
                                        <option value="OE">OE</option>
                                        <option value="OD">OD</option>
                                    </select>
                                    <select class="form-control col" name="ad-visao[0]">
                                        <option value="Todos">Todos</option>
                                        <option value="Longe">Longe</option>
                                        <option value="Perto">Perto</option>
                                    </select>
                                    <input class="form-control col" type="text" name="adicao[]" placeholder="Adição">
                                </div>


                                <?php
                                    $adicao =   '<li class="input-group col mb-3">'.
                                                    '<select class="form-control col" name="ad-olho[]">'.
                                                        '<option value="OE e OD">OE e OD</option>'.
                                                        '<option value="OE">OE</option>'.
                                                        '<option value="OD">OD</option>'.
                                                    '</select>'.
                                                    '<select class="form-control col" name="ad-visao[]">'.
                                                        '<option value="Todos">Todos</option>'.
                                                        '<option value="Longe">Longe</option>'.
                                                        '<option value="Perto">Perto</option>'.
                                                    '</select>'.
                                                    '<input class="form-control col" type="text" name="adicao[]" placeholder="Adição">'.
                                                '</li>';
                                ?>
                            </ul>
                            <div class="text-center">
                                <button type="button" class="btn btn-sm btn-outline-success mb-3 addAD">Adicionar</button>
                                <button type="button" class="btn btn-sm btn-outline-danger mb-3 rmvAD">Remover</button>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="mt-5 text-center"><button type="submit" class="btn btn-primary" style="width: 30%;">Salvar</button></div>

            </form>
        </div>

        <footer class="bg-dark text-light text-center container-fluid" style="margin-top: 40px">Sistema desenvolvido para fins acadêmicos<br> &copy; Copyright - 2018 By SOR</footer>
        <!-- JavaScript (Opcional) -->
        <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
        <script src="../bootstrap/js/validate.js"></script>
        <script src="../js/jquery/jquery-3.2.1.min.js"></script>
        <script>
            if (<?php if(isset($_SESSION['cadRctTrue'])){echo 'true'; unset($_SESSION['cadRctTrue']);} else {echo 'false';} ?>) {
                $('.alertS').removeClass('hide');
                $('.alertS').delay(1500).fadeOut(1000);
            }

            if (<?php if(isset($_SESSION['cadRctFalse'])){echo 'true'; unset($_SESSION['cadRctFalse']);} else {echo 'false';} ?>) {
                $('.alertE').removeClass('hide');
                $('.alertE').delay(1500).fadeOut(1000);
            }
        </script>
        <script src="../bootstrap-chosen/bootstrap-chosen.js"></script>
        <script src="../js/jquery.mask.js"></script>
        <script>
            $(function() {
                $('.chosen-select').chosen();
                $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
            });

            var cont = 0;
            $('.addAD').click(function () {
                $('.addAdicao').fadeIn(100);
                $('.addAdicao').removeClass('hide');
                if ((cont < 4) && (cont > 0)) {$('.addAdicao').append('<?php echo $adicao; ?>')}
                if (cont < 4) {cont = cont + 1;}
            });
            $('.rmvAD').click(function () {
                if(cont > 0){cont = cont - 1; $('.addAdicao li:last').remove();}
                if(cont == 0){$('.addAdicao').fadeOut(100);}
            });

            var cont2 = 0;
            $('.addDP').click(function () {
                $('.addDP-DNP').fadeIn(100);
                $('.addDP-DNP').removeClass('hide');
                if ((cont2 < 4) && (cont2 > 0)) {$('.addDP-DNP').append('<?php echo $dp; ?>')}
                if (cont2 < 4) {cont2 = cont2 + 1;}
            });
            $('.rmvDP').click(function () {
                if(cont2 > 0){cont2 = cont2 - 1; $('.addDP-DNP li:last').remove();}
                if(cont2 == 0){$('.addDP-DNP').fadeOut(100);}
            });


            $(document).ready(function () {
                $('#vlr').mask('#.##0,00', {reverse: true});
            })
        </script>
        <script src="../js/popper/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>