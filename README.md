## Sistema para gestão de Receitas Ópticas

- Possui funcionalidades básicas como cadastrar usuários e receitas para cada usuários, gerar relatórios e alertar a empresa de quando um cliente estiver com sua receita próximo a vencer.

- Foi utilizado **HTML, CSS, JavaScript, Jquery, Bootstrap, PHP e MySQL**.