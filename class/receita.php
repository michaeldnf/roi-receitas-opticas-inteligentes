<?php
    class receita {
        //Atributos

        //Metodos
        public function cadastrarReceita($cliente_id,$data,$problema_ocular,$doença,$lente,$preco,$obs,$lod_esferico,$lod_cilindrico,$lod_eixo,$loe_esferico,$loe_cilindrico,$loe_eixo,$pod_esferico,$pod_cilindrico,$pod_eixo,$poe_esferico,$poe_cilindrico,$poe_eixo){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "INSERT INTO tb_receita (data_receita,problema_ocular,obs_doenca,lente,preco,observacao,lod_esferico,lod_cilindrico,lod_eixo,loe_esferico,loe_cilindrico,loe_eixo,pod_esferico,pod_cilindrico,pod_eixo,poe_esferico,poe_cilindrico,poe_eixo,cliente_id) VALUES ('$data','$problema_ocular','$doença','$lente','$preco','$obs','$lod_esferico','$lod_cilindrico','$lod_eixo','$loe_esferico','$loe_cilindrico','$loe_eixo','$pod_esferico','$pod_cilindrico','$pod_eixo','$poe_esferico','$poe_cilindrico','$poe_eixo',$cliente_id)";
            $result = mysqli_query($link,$sql);
            if ($result){
                $sql = "SELECT LAST_INSERT_ID() FROM tb_receita";
                $result = mysqli_query($link,$sql);
                $result = mysqli_fetch_array($result);
                mysqli_close($link);
                return $result[0];
            } else {return false;}
        }

        public function cadastrarAdicao($olho, $visao, $adicao, $receita_id){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "INSERT INTO tb_receita_adicao (olho, visao, adicao, receita_id) VALUES('$olho','$visao','$adicao', $receita_id)";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result){
                return true;
            } else {return false;}
        }

        public function cadastrarDp($olho, $visao, $dp, $receita_id){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "INSERT INTO tb_receita_dp (olho, visao, dp, receita_id) VALUES('$olho','$visao','$dp', $receita_id)";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result){
                return true;
            } else {return false;}
        }

        public function buscarReceita($cliente_id){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SELECT * FROM tb_receita WHERE cliente_id = $cliente_id";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result != false){
                $result = mysqli_fetch_all($result);
                return $result;
            } else {return false;}
        }

        public function buscarAdicaoReceita($receita_id){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SELECT olho,visao,adicao FROM tb_receita_adicao WHERE receita_id = $receita_id";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result != false) {
                $result = mysqli_fetch_all($result);
                return $result;
            } else {return false;}
        }

        public function buscarDpReceita($receita_id){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SELECT olho,visao,dp FROM tb_receita_dp WHERE receita_id = $receita_id";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result != false) {
                $result = mysqli_fetch_all($result);
                return $result;
            } else {return false;}
        }

        public function deletarReceita($id_receita){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SELECT id_dp FROM tb_receita_dp WHERE receita_id = $id_receita";
            $result = mysqli_query($link,$sql);
            if ($result){
                $sql = "DELETE FROM tb_receita_dp WHERE receita_id = $id_receita";
                $result = mysqli_query($link,$sql);
            }
            $sql = "SELECT id_adicao FROM tb_receita_adicao WHERE receita_id = $id_receita";
            $result = mysqli_query($link,$sql);
            if ($result){
                $sql = "DELETE FROM tb_receita_adicao WHERE receita_id = $id_receita";
                $result = mysqli_query($link,$sql);
            }
            $sql = "DELETE FROM tb_receita WHERE id_receita = $id_receita";
            $result = mysqli_query($link,$sql);
            return $result;
        }
    }
?>