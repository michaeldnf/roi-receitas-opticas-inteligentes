<?php
    class conexaoBD {
        private $host = 'localhost';
        private $usuario = 'root';
        private $senha = '';
        private $database = 'bd_sor_system';

        function conecta_mysql(){
            $con = mysqli_connect($this->host,$this->usuario,$this->senha,$this->database);
            mysqli_set_charset($con, 'utf8');

            if (mysqli_connect_errno()){
                echo 'Erro ao conectar ao banco: '.mysqli_connect_error();
            }

            return $con;
        }
    }


?>