<?php
    class cliente {
        //Atributos
        private $nome;
        private $sexo;
        private $data_nascimento;
        private $cpf;
        private $celular;
        private $id_endereco;
        private $observacao;

        //Métodos
        public function cadastrarEndereco($id_cidade, $cep, $endereco, $numero){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "INSERT INTO tb_endereco (endereco,numero,cep,cidade_id) VALUES ('$endereco',$numero,'$cep',$id_cidade)";
            $result = mysqli_query($link, $sql);
            if($result){
                $sql = "SELECT LAST_INSERT_ID() FROM tb_endereco";
                $result = mysqli_query($link,$sql);
                $id = mysqli_fetch_array($result);
                $this->id_endereco = $id[0];
                mysqli_close($link);
                return true;
            }else {mysqli_close($link); return false;}

        }


        public function cadastrarCliente($nome, $sexo, $data_nascimento, $cpf, $celular, $observacao, $id_usuario){
            $this->nome = $nome; $this->sexo = $sexo; $this->data_nascimento = $data_nascimento;
            $this->cpf = $cpf; $this->celular = $celular; $this->observacao = $observacao;
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "INSERT INTO tb_cliente (nome_cliente, sexo, data_nascimento, cpf, celular, observacao, endereco_id, usuario_id) VALUES ('$this->nome', '$this->sexo', '$this->data_nascimento', '$this->cpf', '$this->celular', '$this->observacao', $this->id_endereco, $id_usuario)";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result){
                return true;
            }else return false;
        }

        public function getAI(){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SHOW TABLE STATUS LIKE 'tb_cliente'";
            $result = mysqli_query($link,$sql);
            $result = mysqli_fetch_array($result);
            $id = $result['Auto_increment'];
            mysqli_close($link);
            return $id;
        }

        public function buscarCliente($busca){
            require_once ('conexaoBD.php');
            session_start();
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $usuario_id = $_SESSION['id_usuario'];
            $test = (int) $busca;
            if ($test != 0){
                $sql = "SELECT id_cliente, nome_cliente, cpf, celular FROM tb_cliente WHERE id_cliente = $test AND usuario_id = $usuario_id";
            } else {
                $sql = "SELECT id_cliente, nome_cliente, cpf, celular FROM tb_cliente WHERE nome_cliente LIKE '%$busca%' AND usuario_id = $usuario_id";
            }
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            return mysqli_fetch_all($result);
        }

        public function ultimosCadastros($limite){
            require_once ('conexaoBD.php');
            $usuario_id = $_SESSION['id_usuario'];
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SELECT id_cliente, nome_cliente, cpf, celular FROM tb_cliente WHERE usuario_id = $usuario_id ORDER BY id_cliente DESC LIMIT $limite";
            $result = mysqli_query($link,$sql);
            mysqli_close($link);
            if ($result != false){
                return mysqli_fetch_all($result);
            } else {return false;}
        }

        public function deletarCliente($id_cliente){
            require_once ('conexaoBD.php');
            $bd = new conexaoBD();
            $link = $bd->conecta_mysql();
            $sql = "SELECT r.id_receita FROM tb_receita r INNER JOIN tb_cliente c WHERE r.cliente_id = c.id_cliente AND c.id_cliente = $id_cliente";
            $result = mysqli_query($link,$sql);
            $test = true;
            if ($result != false){
                require_once ('receita.php');
                $rct = new receita();
                $result = mysqli_fetch_all($result);
                foreach ($result AS $id){
                    $test = $rct->deletarReceita($id[0]);
                }
            }
            if ($test){
                $bd = new conexaoBD();
                $link = $bd->conecta_mysql();
                $sql = "SELECT endereco_id FROM tb_cliente WHERE id_cliente = $id_cliente";
                $endereco_id = mysqli_query($link,$sql);
                $sql = "DELETE FROM tb_cliente WHERE id_cliente = $id_cliente";
                $result = mysqli_query($link, $sql);
                if ($result and $endereco_id != false){
                    $endereco_id = mysqli_fetch_array($endereco_id);
                    $endereco_id = $endereco_id['endereco_id'];
                    $sql = "DELETE FROM tb_endereco WHERE id_endereco = $endereco_id";
                    $result = mysqli_query($link, $sql);
                    return true;
                } else {return false;}
            } else {return false;}
        }

    }
?>